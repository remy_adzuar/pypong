import pygame
from config import Config
import inputhandlers
import physics
import message
from racket import Racket
from ball import Ball
from ia import IA

# utilitaires

def offset(config:Config, coords):
    offset = config.get_offset()
    offset_coords = [coords[0]+offset[0],
    coords[1]+offset[1],
    coords[2],coords[3]]

    return offset_coords

# Fonction d'etat de jeu

def game(config: Config):
    screen = config.get_screen()
    clock = config.get_clock()

    #Init object of the game
    racketPlayer = Racket([16,672//2])
    ball = Ball([1216//2, 672//2])
    racketIA = Racket([1184,672//2]) # 1200 = 1216-16
    ia = IA(racketIA, ball)
    borderUp = (0,0,1216,0)
    borderDown = (0,672,1216,672)

    score = [0,0]

    loop = True
    while loop:
        
        #Debug zone
        # print(ia.ball.pos, ia.racket.pos)
 

        # Physics
        if ball.check_collide(racketIA.get_coords()):
            config.play_sound("beep")
            ball.lift(racketIA.get_coords())
            ia.target()

        if ball.check_collide(racketPlayer.get_coords()):
            ball.lift(racketPlayer.get_coords())
            config.play_sound("beep")

        if ball.check_collide(borderUp) or ball.check_collide(borderDown):
            config.play_sound("rebound")
            ball.border_rebound()
        
        ball.move_pos()
        if config.getIA():
            ia.move_to_ball()

        if not physics.check_border(ball.get_pos(), score):
            config.play_sound("victory")
            ball = Ball([1216//2, 672//2])
            ia.ball = ball
        
        # Game Logic
        target_score = 3
        if score[0] > target_score-2 and score[1] > target_score-2:
            if score[0] > score[1]+1 and score[1] >= target_score-1:
                return "VICTORY"
            elif score[1] > score[0]+1 and score[0] >= target_score-1:
                return "DEFEAT"
        else:
            if score[0] == target_score:
                state_victory(config)
                return "VICTORY"
            elif score[1] == target_score:
                state_defeat(config)
                return "DEFEAT"

        # Inputs Du jeu
        input_res = inputhandlers.game_input()
        if input_res == "QUIT":
            return "QUIT"
        if config.getIA():
            if input_res == "W":
                racketPlayer.move_pos([0,-4])
            if input_res == "S":
                racketPlayer.move_pos([0,4])
        
        if not config.getIA():
            input_res = inputhandlers.game_adv_input()
            for inp in input_res:
                if inp == "W":
                    racketPlayer.move_pos([0,-4])
                if inp == "S":
                    racketPlayer.move_pos([0,4])
                if inp == "UP":
                    racketIA.move_pos([0,-4])
                if inp == "DOWN":
                    racketIA.move_pos([0,4])
        #IA movement

        #Drawing
        screen.fill((0,0,0))
        pygame.draw.rect(screen,(255,255,255), offset(config,ball.get_rect_size()))
        pygame.draw.rect(screen,(255,255,255), offset(config,racketPlayer.get_rect_size()))
        pygame.draw.rect(screen,(255,255,255), offset(config,racketIA.get_rect_size()))
        pygame.draw.rect(screen,(255,255,255), offset(config,(1216//2-4,0, 8, 672)))
        pygame.draw.rect(screen,(255,255,255), offset(config,(0,0,1216,672)),1)

        # Text
        message.print_score(config,score)

        pygame.display.update()

        clock.tick(60)

def state_choix_adv(config:Config):

    loop = True
    result = None
    while loop:

        input_res = inputhandlers.state_choix_adv()
        if input_res == "QUIT":
            return "QUIT"
        if input_res == "IA":
            result = game(config)
        if input_res == "HUMAN":
            config.setHuman()
            result = game(config)
        
        if result != None:
            if result == "QUIT":
                return "QUIT"
            if result == "VICTORY":
                return "VICTORY"
            if result == "DEFEAT":
                return "DEFEAT"

        config.clear()
        message.print_screen_text(config, message.text_choix_adv)
        pygame.display.update()

def state_victory(config):
    loop = True
    while loop:

        input_res = inputhandlers.wait_press()
        if input_res == "QUIT":
            return "QUIT"
        if input_res == "KEYDOWN":
            return "KEYDOWN"
        
        config.clear()
        message.print_screen_text(config, message.text_victory)
        pygame.display.update()

def state_defeat(config):
    loop = True
    while loop:

        input_res = inputhandlers.wait_press()
        if input_res == "QUIT":
            return "QUIT"
        if input_res == "KEYDOWN":
            return "KEYDOWN"
        
        config.clear()
        message.print_screen_text(config, message.text_defeat)
        pygame.display.update()

def main():

    screen = pygame.display.set_mode((1280,720))
    clock = pygame.time.Clock()
    font = pygame.font.Font("freesansbold.ttf", 32)
    offset = [32,24]

    config = Config(screen, clock, font, offset)
    # Ajout des sons
    # config.add_sound("beep","./sounds/beep.wav")
    # config.add_sound("rebound", "./sounds/rebound.wav")
    config.add_sound("victory", "./sounds/victory.wav")

    config.add_sound("beep","./sounds/c7bep.wav")
    config.add_sound("rebound", "./sounds/c5bep.wav")
    # config.add_sound("victory", "./sounds/c6bep.wav")

    loop = True
    while loop:
        config.clear()

        input_res = inputhandlers.state_menu()
        if input_res == "QUIT" or input_res == "3":
            pygame.quit()
            break
        if input_res == "1":
            # Direction selection IA/Humain
            result = state_choix_adv(config)
            if result == "QUIT":
                pygame.quit()
                break
            if result == "VICTORY":
                pass
            if result == "DEFEAT":
                pass
        if input_res == "2":
            print("NOT WORKING")

        
        message.print_screen_text(config, message.text_menu)
        pygame.display.update()


# Lancement du jeu
if __name__ == "__main__":

    pygame.init()

    main()