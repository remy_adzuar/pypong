class Ball:

    count = 0

    def __init__(self, pos) -> None:

        self.pos = pos
        self.size = [16,16]
        self.speed = 1

        # Change le sens de service selon le nombre de balle jouee
        if self.__class__.count%4 < 2:
            self.direction = [-4,4]
        else:
            self.direction = [4,4]
        self.__class__.count += 1


    
    def get_pos(self):
        return self.pos
    
    def get_rect_size(self):
        return (self.pos[0], self.pos[1], self.size[0], self.size[1])

    def set_pos(self, pos):
        self.pos = pos
    
    def move_pos(self):
        self.pos[0] += self.direction[0]
        self.pos[1] += self.direction[1]

    def get_coords(self):
        x1 = self.pos[0]
        y1 = self.pos[1]
        x2 = self.pos[0]+self.size[0]
        y2 = self.pos[1]+self.size[1]
        return [x1,y1,x2,y2]
    
    def check_collide(self, coords):
        self_coords = self.get_coords()
        x_overlap = min(self_coords[2],coords[2]) >= max(self_coords[0],coords[0])
        y_overlap = min(self_coords[3],coords[3]) >= max(self_coords[1],coords[1])
        return x_overlap and y_overlap
    
    def change_speed(self, coef):
        self.speed = self.speed*coef

    def reverse(self):
        self.direction = [self.direction[0]*-1,self.direction[1]*-1]
    
    def lift(self, coords):
        
        if self.check_collide(coords):
            self.reverse()
            self.change_speed(1.25)

            self_coords = self.get_coords()
            size_racket = coords[3] - coords[1] # Taille de la raquette
            middle_racket = size_racket//2
            middle_ball = self.size[1]//2

            y_ball = self_coords[1]+middle_ball
            y_racket = coords[1]+middle_racket
            delta = y_racket-y_ball
            lift = delta//8
            self.direction[1]= -lift
            if self.direction[0]<0:
                self.direction[0] = self.direction[0]-self.speed
            else:
                self.direction[0] = self.direction[0]+self.speed
            return True
        return False
    
    def border_rebound(self):
        self.direction = [self.direction[0],self.direction[1]*-1]

if __name__ == "__main__":

    ball1 = Ball([0,0])
    ball2 = Ball([15,18])
    print(ball1.get_coords())
    print(ball2.get_coords())

    print(ball1.check_collide(ball2.get_coords()))