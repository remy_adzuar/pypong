class Racket:

    def __init__(self, pos) -> None:
        self.pos = pos
        self.size = [16,80]
    
    def get_pos(self):
        return self.pos
    
    def get_rect_size(self):
        return (self.pos[0], self.pos[1], self.size[0], self.size[1])
    
    def set_pos(self, pos):
        self.pos = pos
    
    def move_pos(self, move):
        self.pos[0] += move[0]
        self.pos[1] += move[1]
        self.check_border()

    def check_border(self):
        if self.pos[1] < 0:
            self.pos[1] = 0
        if self.pos[1]+self.size[1] > 672:
            self.pos[1] = 672-self.size[1]

    def get_coords(self):
        x1 = self.pos[0]
        y1 = self.pos[1]
        x2 = self.pos[0]+self.size[0]
        y2 = self.pos[1]+self.size[1]
        return [x1,y1,x2,y2]
    
    def check_collide(self, coords):
        self_coords = self.get_coords()
        x_overlap = min(self_coords[2],coords[2]) >= max(self_coords[0],coords[0])
        y_overlap = min(self_coords[3],coords[3]) >= max(self_coords[1],coords[1])
        return x_overlap and y_overlap

if __name__ == "__main__":

    rack = Racket([0,0])
    print(rack.get_coords())