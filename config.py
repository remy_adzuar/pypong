import pygame

class Config:

    def __init__(self, screen:pygame.Surface, 
    clock:pygame.time.Clock, 
    font:pygame.font.Font,
    offset) -> None:
        self.screen = screen
        self.clock = clock
        self.font = font
        self.offset = offset
        self.ia = True
        self.sounds = {}

    def get_screen(self):
        return self.screen
    
    def get_clock(self):
        return self.clock
    
    def get_font(self):
        return self.font
    
    def get_offset(self):
        return self.offset
    
    def clear(self):
        self.screen.fill((0,0,0))
    
    def setHuman(self):
        self.ia = False
    
    def getIA(self):
        return self.ia
    
    def add_sound(self,name:str, urlSound:str):
        self.sounds[name] = pygame.mixer.Sound(urlSound)
    
    def get_sound(self, name:str):
        return self.sounds[name]
    
    def play_sound(self, name:str):
        pygame.mixer.Sound.play(self.get_sound(name))