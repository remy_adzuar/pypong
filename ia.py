from ball import Ball
from racket import Racket

# fichier pour gerer l'ia du jeu
import random

class IA:

    def __init__(self, racket:Racket, ball:Ball) -> None:
        self.racket = racket
        self.ball = ball
        self.target()

    def target(self):
        delta = random.randrange(-42,42)
        self._target = delta

    def move_to_ball(self):

        pos_ball = self.ball.get_coords()
        size_ball = pos_ball[3]-pos_ball[1]
        middle_ball = pos_ball[1]+size_ball//2

        pos_rack = self.racket.get_coords()
        size_racket = pos_rack[3]-pos_rack[1]
        middle_racket = pos_rack[1]+size_racket//2

        middle_racket+= self._target

        if middle_racket > middle_ball:
            if middle_racket-8 < middle_ball:
                self.racket.move_pos([0,-1])
            else:
                self.racket.move_pos([0,-4])
        else:
            if middle_racket+8 > middle_ball:
                self.racket.move_pos([0,1])
            else:
                self.racket.move_pos([0,4])