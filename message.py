import pygame
from config import Config

WHITE = (255,255,255)
GREY = (100,100,100)

test = [{
    "center": True,
    "content": "Appuyez sur une touche pour lancer une partie",
    "pos": (1280//2, 720//2)
}]

text_menu = [{
    "center":True,
    "content": "1. Nouvelle Partie",
    "pos": (1280//2,720//2-64)
},{
    "center":True,
    "content": "2. Tableau de Score (not working)",
    "pos": (1280//2, 720//2)
},{
    "center":True,
    "content": "3. Quitter le Programme",
    "pos": (1280//2, 720//2+64)
}]

text_choix_adv = [{
    "center":True,
    "content": "1. IA / CPU",
    "pos": (1280//2,720//2-64)
},{
    "center":True,
    "content": "2. HUMAN (Not working yet)",
    "pos": (1280//2, 720//2)
}]

text_victory = [{
    "center": True,
    "content": "BRAVO",
    "pos": (1280//2, 720//2-32)
},{
    "center": True,
    "content": "Vous Avez gagne la partie",
    "pos": (1280//2, 720//2+32)
}]

text_defeat = [{
    "center": True,
    "content": "DOMMAGE",
    "pos": (1280//2, 720//2-32)
},{
    "center": True,
    "content": "Vous Avez perdu la partie",
    "pos": (1280//2, 720//2+32)
}]


def print_score(config:Config, score):
    font = config.get_font()
    screen = config.get_screen()
    text = "J:"+str(score[0])+"#"+str(score[1])+":IA"
    rendu = font.render(text, True, WHITE, GREY)
    screen.blit(rendu, (0,0))

def print_screen_text(config:Config,texts):

    font = config.get_font()
    screen = config.get_screen()
    
    for text in texts:
        temp = font.render(text["content"], True, WHITE, GREY)
        if (text["center"]):
            rect = temp.get_rect()
            rect.center = text["pos"]
            screen.blit(temp, rect)
        else:
            screen.blit(temp, text["pos"])
        