# Projet PyPong

Remake de Pong avec Python3 et Pygame

## Fonctionnalites :

La partie se deroule sur un cour de tennis de table
Un Joueur affronte un autre joueur ou une IA
Selection du mode de jeu, par defaut le mode de score classique au tennis de table
La balle doit reagir a la vitesse de la raquette

## Previsions :

5 x 30 min pour coder un prototype fonctionnel
5 x 30 min pour paufiner le prototype

10 min de preparation papier
10 min de preparation PC

## Idees :

Mettre plusieurs type de balle
Possibilite de push pour faire un smash
Ajout d'un mode de jeu avec une barre d'energie
Pouvoir enregistrer des score et consulter un tableau de highscore
Ajout de sons
Implementer differente physique pour la raquette ou les balles
(exemple balle a energie negative qui accelere en perdant de l'energie)

## Suivis :

1 ere session

25 min, Pygame set et boucle de gameplay principale
5 min, pause
25 min, Set up Object Racket et Ball, Mouvement du Racket joueur
5 min, pause
25 min, Collision Ball et Racket et Ball et Border ok, Mouvement balle OK

Fin 1 ere session

2 eme Session

25 min, Implementation physique basique de la balle OK, set up IA
5 min, pause
25 min, Implementation IA ok, Correction de bug.
Fin prototype

Fin 2 eme session

3 eme Session

25 min, Implementation du Score et Affichage du score, changement de sens pour le service de balle, et physique de la balle amelioree
5 min, pause
25 min, Fix racket IA, Ajout visuel bordure de terrain, ajout ecran introductif
5 min, pause
25 min, ecran intro ok, ajout mecanisme de victoire et defaite
5 min, pause
25 min, Implementation choix de l'adversaire entre humain et IA
5 min, pause
25 min, Ajour ecran de victoire et de defaite, ajout retour au menu principale apres fin de partie

Fin de projet

Point a ameliorer :

L'estimation du temps de chaque tache, finalement faire un pong en 5 heure de temps c'est tres court, et je n'ai encore une fois pas pu ajouter de point avec les idees supplementaire

La gestion des differents ecran doit etre ameliorer car la lisibilite de ces dernier est assez mauvaise