import pygame

# Fichier qui contient les different handler pour le comportement des input selon l'etat du jeu

def game_input():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return "QUIT"

    keys = pygame.key.get_pressed()

    if keys[pygame.K_w]:
        return "W"
    if keys[pygame.K_s]:
        return "S"

def game_adv_input():
    result = []
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return "QUIT"
    
    keys = pygame.key.get_pressed()
    
    if keys[pygame.K_w] and not keys[pygame.K_s]:
        result.append("W")
    if keys[pygame.K_s] and not keys[pygame.K_w]:
        result.append("S")

    if keys[pygame.K_UP] and not keys[pygame.K_DOWN]:
        result.append("UP")
    if keys[pygame.K_DOWN] and not keys[pygame.K_UP]:
        result.append("DOWN")

    return result

def wait_press():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return "QUIT"
        if event.type == pygame.KEYDOWN:
            return "KEYDOWN"

def state_menu():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return "QUIT"
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_1:
                return "1"
            if event.key == pygame.K_2:
                return "2"
            if event.key == pygame.K_3:
                return "3"

def state_choix_adv():

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return "QUIT"
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_1:
                return "IA"
            if event.key == pygame.K_2:
                return "HUMAN"
